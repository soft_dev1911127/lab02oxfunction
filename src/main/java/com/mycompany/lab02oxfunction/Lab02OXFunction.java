/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab02oxfunction;

import java.util.Scanner;

/**
 *
 * @author kantinan
 */
public class Lab02OXFunction {

    static char[][] table = {{'_', '_', '_'}, {'_', '_', '_'}, {'_', '_', '_'}};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;

    private static void printWelcome() {
        System.out.println("Tic-Tac-Toe!");
    }

    private static void printTable() {
        System.out.println("Current board layout :");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println(" ");
        }
    }

    private static void printTurn() {
        System.out.println("Player " + currentPlayer + ", enter an empty row and column to place your mark!");
    }

    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please in put a row number : ");
            row = sc.nextInt();
            System.out.print("Please in put a column number : ");
            col = sc.nextInt();
            if (table[row - 1][col - 1] == '_') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }

        }

    }
    
    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }
    
    private static boolean isWin() {
        if (checkRow() || checkCol() || checkDiagonal1() || checkDiagonal2()) {
            return true;
        }
        return false;
    }
    
    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }
    
    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }
    
     private static boolean checkDiagonal1() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }
     
    private static boolean checkDiagonal2() {
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }
    
    private static void printWin() {
        System.out.println(currentPlayer + " Win!!!");
    }

    public static void main(String[] args) {
         printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();
            if (isWin()) {
                printTable();
                System.out.println();
                printWin();
                break;
            }
            switchPlayer();
        }
    }

}
